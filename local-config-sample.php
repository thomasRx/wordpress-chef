<?php
/*
This is a sample local-config.php file
In it, you *must* include the four main database defines

You may include other settings here that you only want enabled on your local development checkouts
*/

define( 'DB_NAME', 'putYourDbNameHere' );
define( 'DB_USER', 'putYourDbUserHere' );
define( 'DB_PASSWORD', 'putYourDbPasswordHere' );
define( 'DB_HOST', 'localhost' ); // Probably 'localhost'

// =================================================================
// Debug mode
// Debugging? Enable these.
// =================================================================
// define( 'SAVEQUERIES', true );
// define( 'WP_DEBUG', true );
