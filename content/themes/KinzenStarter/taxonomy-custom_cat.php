<?php get_header(); ?>

            <div class="content row">


                <div class="main large-8 columns" role="main">

                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <article id="post-<?php the_ID(); ?>" class="article" role="article">

                        <header class="article__header">
                            <h1 class="article__header__title"><?php the_title(); ?></h1>
                            <p class="article__header__vcard">
                                <?php printf(__('Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span> <span class="amp">&</span> filed under %4$s.', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')), bones_get_the_author_posts_link(), get_the_category_list(', ')); ?>
                            </p>
                        </header> <!-- end article header -->

                        <div class="article__content">
                            <?php the_content(); ?>
                        </div> <!-- end article section -->

                        <footer class="article__footer">
                            <p class="article__footer__tags"><?php the_tags('<span class="article__footer__tags--title">' . __('Tags:', 'bonestheme') . '</span> ', ', ', ''); ?></p>
                        </footer> <!-- end article footer -->

                        <?php // If comments are open or we have at least one comment, load up the comment template.
                                        if ( comments_open() || get_comments_number() )
                                                comments_template();
                        ?>

                    </article> <!-- end article -->

                    <?php endwhile; ?>

                            <?php if (function_exists('bones_page_navi')) { ?>
                                    <?php bones_page_navi(); ?>
                            <?php } else { ?>
                                    <nav class="wp-prev-next">
                                            <ul class="clearfix">
                                                <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?></li>
                                                <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?></li>
                                            </ul>
                                    </nav>
                            <?php } ?>

                    <?php else : ?>

                            <article id="post-not-found" class="article">
                                    <header class="article__header">
                                        <h1 class="article__header__title"><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
                                    </header>
                                    <div class="article__content">
                                        <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
                                    </div>
                                    <footer class="article__footer">
                                        <p><?php _e("This is the error message in the index.php template.", "bonestheme"); ?></p>
                                    </footer>
                            </article>

                    <?php endif; ?>

                </div> <!-- end #main -->

                <?php get_sidebar(); ?>


            </div> <!-- end #content -->

<?php get_footer(); ?>
