<?php get_header(); ?>

    <div class="content row">

        <div class="main large-12 columns" role="main">

            <article id="post-not-found" class="article">
                    <header class="article__header">
                        <h1 class="article__header__title"><?php _e("Epic 404 - Article Not Found", "bonestheme"); ?></h1>
                    </header>
                    <div class="article__content">
                        <p><?php _e("The article you were looking for was not found, but maybe try looking again!", "bonestheme"); ?></p>
                    </div>
                    <footer class="article__footer">
                        <p><?php get_search_form(); ?></p>
                    </footer>
            </article>

        </div>

    </div>

<?php get_footer(); ?>
