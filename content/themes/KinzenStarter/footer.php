            <footer class="master_foot" role="contentinfo">
                <div class="row">
                    <nav class="master_foot__navigation large-6 columns" role="navigation">
                        <?php bones_footer_links(); ?>
                    </nav>

                    <p class="master_foot__copyright large-6 columns text_right">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. by <a class="master_foot_website_author" href="http://kinzen.com" target="blank">Kinzen</a></p>
                </div>
            </footer> <!-- end .master_foot -->

        </div> <!-- end #page_container -->

        <!-- all js scripts are loaded in library/bones.php -->
        <?php wp_footer(); ?>

    </body>

</html> <!-- end page. what a ride! -->
