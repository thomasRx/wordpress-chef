<section class="comments">
    <?php

    // Do not delete these lines
        if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
            die ('Please do not load this page directly. Thanks!');

        if ( post_password_required() ) { ?>
            <div class="alert alert-help">
                <p class="nocomments"><?php _e("This post is password protected. Enter the password to view comments.", "bonestheme"); ?></p>
            </div>
        <?php
            return;
        }
    ?>

    <!-- You can start editing here. -->

    <?php if ( have_comments() ) : ?>
        <h3 class="comments__title"><?php comments_number(__('<span>No</span> Responses', 'bonestheme'), __('<span>One</span> Response', 'bonestheme'), _n('<span>%</span> Response', '<span>%</span> Responses', get_comments_number(),'bonestheme') );?> to &#8220;<?php the_title(); ?>&#8221;</h3>
        <hr />

        <nav class="comments__nav">
            <ul class="clearfix">
                    <li><?php previous_comments_link() ?></li>
                    <li><?php next_comments_link() ?></li>
            </ul>
        </nav>

        <ol class="comments__list">
            <?php wp_list_comments('type=comment&callback=bones_comments'); ?>
        </ol>

        <nav class="comments__nav">
            <ul class="clearfix">
                    <li><?php previous_comments_link() ?></li>
                    <li><?php next_comments_link() ?></li>
            </ul>
        </nav>

        <?php else : // this is displayed if there are no comments so far ?>

        <?php if ( comments_open() ) : ?>
                <!-- If comments are open, but there are no comments. -->

        <?php else : // comments are closed ?>

        <!-- If comments are closed. -->
        <!--p class="nocomments"><?php _e("Comments are closed.", "bonestheme"); ?></p-->

        <?php endif; ?>

    <?php endif; ?>


    <?php if ( comments_open() ) : ?>

    <section class="comment__respond">

        <h3 class="comment__respond__title"><?php comment_form_title( __('Leave a Reply', 'bonestheme'), __('Leave a Reply to %s', 'bonestheme' )); ?></h3>
        <hr />
        <div id="comment__respond__cancelCommentReply">
            <p><?php cancel_comment_reply_link(); ?></p>
        </div>

        <?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
            <div class="comment__respond__alert comment__respond--alert-help">
                <p><?php printf( __('You must be %1$slogged in%2$s to post a comment.', 'bonestheme'), '<a href="<?php echo wp_login_url( get_permalink() ); ?>">', '</a>' ); ?></p>
            </div>
        <?php else : ?>

        <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" class="comment__respond__form">
            <fieldset class="comment__respond__form__fieldset">
                <?php if ( is_user_logged_in() ) : ?>

                <p class="comment__respond__form__logged-in-as"><?php _e("Logged in as", "bonestheme"); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e("Log out of this account", "bonestheme"); ?>"><?php _e("Log out", "bonestheme"); ?> <?php _e("&raquo;", "bonestheme"); ?></a></p>

                <?php else : ?>

                <ul class="comment__respond__form__form_Elements clearfix">

                    <li class="form_Elements__item row">
                        <div class="large-12 columns">
                            <label for="author"><?php _e("Name", "bonestheme"); ?> <?php if ($req) : ?><span class="form_Elements__item--req">*</span><?php endif; ?></label>
                            <input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" placeholder="<?php _e('Your Name*', 'bonestheme'); ?>" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
                        </div>
                    </li>

                    <li class="form_Elements__item row">
                        <div class="large-12 columns">
                            <label for="email"><?php _e("Mail", "bonestheme"); ?> <?php if ($req) : ?><span class="form_Elements__item--req">*</span><?php endif; ?></label>
                            <input type="email" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" placeholder="<?php _e('Your E-Mail*', 'bonestheme'); ?>" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
                            <small><?php _e("(will not be published)", "bonestheme"); ?></small>
                        </div>
                    </li>

                    <li class="form_Elements__item row">
                        <div class="large-12 columns">
                            <label for="url"><?php _e("Website", "bonestheme"); ?></label>
                            <input type="url" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" placeholder="<?php _e('Got a website?', 'bonestheme'); ?>" tabindex="3" />
                        </div>
                    </li>

                </ul>

                <?php endif; ?>

                <ul class="comment__respond__form__form_Elements clearfix">
                    <li class="form_Elements__item row">
                        <div class="large-12 columns">
                            <textarea name="comment" id="comment" placeholder="<?php _e('Your Comment here...', 'bonestheme'); ?>" tabindex="4"></textarea>
                        </div>
                    </li>

                    <li class="form_Elements__item row">
                        <div class="large-12 columns">
                            <input name="submit" type="submit" id="submit" class="button button-small" tabindex="5" value="<?php _e('Submit', 'bonestheme'); ?>" />
                            <?php comment_id_fields(); ?>
                        </div>
                    </li>
                    <li class="form_Elements__item row">
                        <div class="comment__respond__form__alert comment__respond__form--alert-info large-12 columns">
                            <p class="allowed_tags alert-box"><strong>XHTML:</strong> <?php _e('You can use these tags', 'bonestheme'); ?>: <code><?php echo allowed_tags(); ?></code></p>
                        </div>
                    </li>
            </ul>
                <?php do_action('comment_form', $post->ID); ?>
            </fiedlset>
        </form>

        <?php endif; // If registration required and not logged in ?>
    </section>

    <?php endif; // if you delete this the sky will fall on your head ?>
</section>
