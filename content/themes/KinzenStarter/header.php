<!doctype html>

<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

    <head>
        <meta charset="utf-8">

        <!-- Google Chrome Frame for IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?php wp_title(''); ?></title>

        <!-- mobile meta (hooray!) -->
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
        <!--[if IE]>
            <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <![endif]-->
        <!-- or, set /favicon.ico for IE10 win -->
        <meta name="msapplication-TileColor" content="#f01d4f">
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

        <!-- wordpress head functions -->
        <?php wp_head(); ?>
        <!-- end of wordpress head -->

    </head>

    <body <?php body_class(); ?>>

        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- Master Head Module -->

        <div id="page_container">

            <header class="master_head" role="banner">
                <div class="row">
                    <div class="large-12 columns">
                        <!-- to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> -->
                        <div class="master_head__brand" >
                            <!-- If you want to use a logo image change the code above by this : -->
                            <figure>
                                <a href="<?php echo home_url(); ?>" rel="nofollow"><img class="master_head__brand__logo" src="<?php bloginfo('template_directory') ?>/library/images/master_head__logo.png" width="106" height="120" alt="Logo of <?php bloginfo('name'); ?>" /></a>
                            </figure>
                            <h1 class="master_head__brand_name">
                                <a href="<?php echo home_url(); ?>" rel="nofollow">
                                    <?php bloginfo('name'); ?>
                                </a>
                            </h1>
                        </div>

                        <!-- if you'd like to use the site description you can un-comment it below -->
                        <?php // bloginfo('description'); ?>

                        <nav class="master_head__navigation" role="navigation">
                            <?php bones_main_nav(); ?>
                        </nav>
                    </div>
                </div>
            </header> <!-- end .master_head -->
