module.exports = function(grunt) {
    'use strict';

    // Load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON( 'package.json' ),

        // style (Sass) compilation via Compass
        compass: {
            dist: {
                options: {
                    config: 'library/scss/config.rb',
                    cssDir: 'library/css',
                    sassDir: 'library/scss',
                    javascriptsDir : 'library/js',
                    imagesDir: 'library/images',
                    fontsDir: "library/fonts",
                    environment: 'production',
                    force: true
                }
            },
            watch: {
              options: {
                    config: 'library/scss/config.rb',
                    cssDir: 'library/css',
                    sassDir: 'library/scss',
                    javascriptsDir : 'library/js',
                    imagesDir: 'library/images',
                    fontsDir: "library/fonts",
                    environment: 'development'
                }
            }
        },

        // concatenation and minification all in one
        uglify: {
            dist: {
                options: {
                    banner: '/*! <%= pkg.name %> scripts - version: <%= pkg.version %> - Date: <%= grunt.template.today("yyyy-mm-dd") %> - Author: <%= pkg.author.name %>  */\n',
                    sourceMap: 'library/js/map/source-map-main.js.map'
                },
                files: {
                    'library/js/build/scripts.min.js': [ 'library/js/scripts.js' ]
                }
            }
        },

        // Run js through jshint
        jshint: {
            files: ['Gruntfile.js', 'library/js/scripts.js'],
            options: {
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },

        imagemin: {
            png: {
                options: {
                    optimizationLevel: 7
                },
                files: [
                    {
                        // Set to true to enable the following options…
                        expand: true,
                        // cwd is 'current working directory'
                        cwd: 'library/images/',
                        src: ['**/*.png'],
                        // Could also match cwd line above. i.e. project-directory/img/
                        dest: 'library/images/',
                        ext: '.png'
                    }
                ]
            },
            jpg: {
                options: {
                    progressive: true
                },
                files: [
                    {
                        // Set to true to enable the following options…
                        expand: true,
                        // cwd is 'current working directory'
                        cwd: 'library/images/',
                        src: ['**/*.jpg'],
                        // Could also match cwd. i.e. project-directory/img/
                        dest: 'library/images/',
                        ext: '.jpg'
                    }
                ]
            }
        },

        watch: {
            options: {
            livereload: true
          },
          scss: {
            files: ['library/scss/*.scss', 'library/scss/**/*.scss'],
            tasks: ['compass:watch'],
          },
          js: {
                files: '<%= jshint.files %>',
                tasks: ['jshint', 'uglify']
            },
            livereload: {
                files: ['library/css/style.css', 'library/js/*.js', '*.html', '**/*.php', 'library/images/**/*.{png,jpg,jpeg,gif,webp,svg}']
            }
        }

    });

    // Default task, use for development
    grunt.registerTask('default', ['watch']);

    // Build task, use for production or preproduction
    grunt.registerTask('build', ['compass:dist', 'jshint', 'uglify', 'imagemin']);

};
